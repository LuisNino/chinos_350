<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Ruta de Prueba
Route::get('hola' , function(){
    echo "HOLA";
});

//Arreglos :3
Route:: get('arreglo' , function(){

    //defino un arreglo
    $estudiantes = [ "AN" => "Ana" , 
                     "M" => "Maria" ,   
                     "VA" => "Valeria" , 
                     "CA" => "Carlos" ];

    //Ciclo foreach: recorrer arreglo
    foreach($estudiantes as $indice => $e){
        echo "$e Tiene el Indice: $indice  <br />";
    }

});


//Ruta de Paises
Route::get( 'paises' , function(){

    $paises = [
        "Colombia" => [
                         "Capital" => "Bogotá",
                         "Moneda" => "Pesos",
                         "Población" => 50372424.0,
                         "Ciudades" => [ "Medellin" , "Cali" , "Barranquilla"]
                      ],
        "Perú" => [
                    "Capital" => "Lima",
                    "Moneda" => "Sol",
                    "Población" => 33050325.0,
                    "Ciudades" => [ "Cuzco" , "Trujillo" , "Arequipa"]  
                  ],
        "Ecuador" => [
                        "Capital" => "Quito",
                        "Moneda" => "Dolar",
                        "Población" => 17517141.0,
                        "Ciudades" => [ "Guayaquil" , "Cuenca" , "Manta"]
                     ],
        "Brazil" => [
                        "Capital" => "Brasilia",
                        "Moneda" => "Real",
                        "Población" => 212216052.0,
                        "Ciudades" => [ "Rio de Janeiro" , "Arrecife" , "Bahia"]
                    ], 
        "Argentina" => [
                        "Capital" => "Buenos Aires",
                        "Moneda" => "Peso",
                        "Población" => 45195777.0,
                        "Ciudades" => [ "Córdoba" , "Rosario" , "Mendoza"]
                    ] 
    ];

    //Enviar Datos de los Paises a una Vista
    //Con función view
    return view('paises')
           ->with("paises" , $paises);

});