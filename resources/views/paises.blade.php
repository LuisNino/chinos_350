<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Paises</title>
</head>
<body>
    <h1 class="text-danger"> Lista de Paises </h1>
    <table class="table table-striped"> 
        <thead>
            <th> Pais </th>
            <th> Capital </th>
            <th> Moneda </th>
            <th> Población </th>
            <th> Ciudades </th>
        </thead>
        <tbody>
            <!-- recorro la tabla foreach blade -->
            @foreach( $paises as $pais => $infopais )
                <tr>
                    <td rowspan="3">  {{  $pais  }}  </td>
                    <td rowspan="3">  {{  $infopais["Capital"]  }}  </td>
                    <td rowspan="3">  {{  $infopais["Moneda"]  }}  </td>
                    <td rowspan="3">  {{  $infopais["Población"]  }}  </td>
                    <th class="text-success"> {{   $infopais["Ciudades"][0]   }}</th>
                </tr>
                <tr>
                    <th class="text-success"> {{   $infopais["Ciudades"][1]   }}</th>
                </tr>
                <tr>
                    <th class="text-success"> {{   $infopais["Ciudades"][2]   }}</th>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>
</html>